var gulp = require('gulp');

var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var sassLint = require('gulp-sass-lint');

var babel = require('gulp-babel');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

var svgSymbols = require('gulp-svg-symbols');

var watch = require('gulp-watch');
var livereload = require('gulp-livereload');

gulp.task('default', ['sass', 'css', 'js', 'watch']);

gulp.task('sass', function () {
    return gulp.src('./styles/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([
        autoprefixer({
            browsers: ['> 1%'],
            cascade: false
        })
    ]))
    .pipe(gulp.dest('./'))
    .pipe(livereload());
});

gulp.task('css', function() {
    return gulp.src('./style.css')
        .pipe(minifyCss({keepSpecialComments: 1}))
        .pipe(gulp.dest('./'));
});

gulp.task('js', function() {
    return gulp.src('./static/scripts/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(concat('site.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./static/dist/'))
    .pipe(livereload());
});

gulp.task('lint', function() {
    return gulp.src('./styles/**/*.scss')
    .pipe(sassLint({
            options: {
                configFile: '.sass-lint.yml',
                formatter: 'stylish'
            }
        }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
});

gulp.task('twig', function() {
    return gulp.src('./templates/*.twig')
        .pipe(livereload());
});

gulp.task('sprites', function() {
    return gulp.src('./static/assets/svg/project-icons/*.svg')
    .pipe(svgSymbols({
        svgClassname: 'c-icon--sprites'
    }))
    .pipe(gulp.dest('./static/assets/svg/sprites'));
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('./styles/**/*.scss', ['sass']);
    gulp.watch('./style.css', ['css']);
    gulp.watch('./static/**/*.js', ['js']);
    gulp.watch('./templates/*.twig', ['twig']);
});
