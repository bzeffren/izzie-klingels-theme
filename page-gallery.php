<?php
/**
 * Template Name: Gallery Page
 * Description: A Page Template for showing a gallery of images
 */


$context = Timber::get_context();
$args = array(
        'post_type' => 'work',
        'posts_per_page' => -1
    );
$post = new TimberPost();
$context['post'] = $post;
$context['works'] = Timber::get_posts($args);
Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );
