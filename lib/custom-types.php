<?php
	/**
	 * Post Type: Custom Posts.
	 */

	$labels = array(
		"name" => __( 'Work', '' ),
		"singular_name" => __( 'Work', '' ),
	);

	$args = array(
		"label" => __( 'Work', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "work", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);
	register_post_type( "work", $args );
